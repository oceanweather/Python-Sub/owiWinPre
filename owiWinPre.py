#! /usr/env python
"""
This Python module provides an interfact to Oceanweather Inc's. ascii .WIN/.PRE
wind and pressure field data format. An interface to read .WIN/.PRE files into
xarray objects is included.
"""
import numpy as np
from datetime import datetime
import sys
import os

try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
try:
    import xarray as xr
    import pandas as pd
except:
    print("Unable to import xarray, some functionality may be diminished")

__all__ = [
    "capspeed",
    "datetime",
    "floorspeed",
    "getlatlon",
    "getslp",
    "getuv",
    "iter_array",
    "list_lon_lat_time_slp",
    "list_lon_lat_time_ws",
    "mags",
    "num_col",
    "output_array_fmt",
    "parse_step_header",
    "return_steps_pre",
    "return_steps_win",
    "strip_fileheader",
    "uv2str",
    "iter_file",
    "iter_file_xrdataset",
    "to_nws13_group",
]

output_array_fmt = "%10.4f"
num_col = 8


def strip_fileheader(win):
    # Skip first header line (temporarily)
    l = win.readline()
    # lasttime = l.split()[-1]
    return win, l


def parse_step_header(head):
    """Parse the header string belonging to an individual timestep

    """
    nlat, nlon, dx, dy, olat, olon, time = (
        int(head[5:9]),
        int(head[15:19]),
        float(head[22:28]),
        float(head[31:37]),
        float(head[43:51]),
        float(head[57:65]),
        datetime(
            int(head[68:72]),
            int(head[72:74]),
            int(head[74:76]),
            int(head[76:78]),
            int(head[78:-1]),
        ),
    )
    # print {'nlat': nlat, 'nlon': nlon,
    #         'dx': dx, 'dy': dy,
    #         'olat': olat, 'olon': olon,
    #         'time': time, 'lasttime': head[68:]}
    return {
        "numlat": nlat,
        "numlong": nlon,
        "dlong": dx,
        "dlat": dy,
        "swlat": olat,
        "swlon": olon,
        "time": time,
        "lasttime": head[68:],
    }


def iter_array(win, far):
    for l in range(far):
        for valuestr in win.readline().replace("\n", "").split():
            yield float(valuestr)


def return_steps_win(win):
    """Read through open WIN file handle returning timestep data via a iterable
    generator.

    Args:
        win (file-like object): Open file handle to win.

    Returns:
        Generator: Iter through timesteps, returning (header, (u, v)) tuple

    Examples:
        >>> lons, lats, times, wss = [], [], [], []
        >>> for step in return_steps_win(win):
        >>>     lat, lon = getlatlon(step)
        >>>     time = step[0]["time"]
        >>>     ws = getuv(step)
        >>>     ws = mags(ws)
        >>>     lons.append(lon)
        >>>     lats.append(lat)
        >>>     times.append(time)
        >>>     wss.append(ws)

    """
    while True:
        try:
            head = win.readline()
            # print head, lasttime
            headinfo = parse_step_header(head)
            data = np.fromiter(
                iter_array(
                    win,
                    int(np.ceil(headinfo["numlat"] * headinfo["numlong"] / 8.0)) * 2,
                ),
                dtype=float,
            )
            # print data.shape, int(np.ceil(head['nlat'] * head['nlon'] / 8.)) * 2, int(np.ceil(head['nlat'] * head['nlon'] / 8.)) * 2 * 8  # Run some checks here
            headinfo["text"] = head
            yield (
                headinfo,
                data,
            )  # the yield statement is used to define generators, replacing the return of a function to provide a result to its caller without destroying local variables
        except ValueError:
            break


def return_steps_pre(pre):
    """Read through open PRE file handle returning timestep data via a iterable
    generator.

    Args:
        pre (file-like object): Open file handle to pre.

    Returns:
        Generator: Iter through timesteps, returning (header, pre) tuple

    Examples:
        >>> lons, lats, times, pre = [], [], [], []
        >>> for step in return_steps_win(win):
        >>>     lat, lon = getlatlon(step)
        >>>     time = step[0]["time"]
        >>>     p = getslp(step)
        >>>     lons.append(lon)
        >>>     lats.append(lat)
        >>>     times.append(time)
        >>>     pre.append(p)

    """
    while True:
        try:
            head = pre.readline()
            # print head, lasttime
            headinfo = parse_step_header(head)
            data = np.fromiter(
                iter_array(
                    pre,
                    int(np.ceil(headinfo["numlat"] * headinfo["numlong"] / 8.0)) * 1,
                ),
                dtype=float,
            )
            # print data.shape, int(np.ceil(head['nlat'] * head['nlon'] / 8.)) * 2, int(np.ceil(head['nlat'] * head['nlon'] / 8.)) * 2 * 8  # Run some checks here
            headinfo["text"] = head
            yield (
                headinfo,
                data,
            )  # the yield statement is used to define generators, replacing the return of a function to provide a result to its caller without destroying local variables
        except ValueError:
            break


def list_lon_lat_time_ws(f):
    """Read through open WIN file handle returning timestep data and coords as
    list the length of ntime.

    Args:
        f (file-like object): Open file handle to win.

    Returns:
        tuple: (lons, lats, times, ws)

    Examples:
        >>> lons, lats, times, wspd = list_lon_lat_time_ws(win_path)

    """
    win, mainhead = strip_fileheader(f)
    lons, lats, times, wss = [], [], [], []
    for step in return_steps_win(win):
        lat, lon = getlatlon(step)
        time = step[0]["time"]
        ws = getuv(step)
        ws = mags(ws)
        lons.append(lon)
        lats.append(lat)
        times.append(time)
        wss.append(ws)
    return lons, lats, times, wss


def list_lon_lat_time_slp(f):
    """Read through open PRE file handle returning timestep data and coords as
    list the length of ntime.

    Args:
        f (file-like object): Open file handle to pre.

    Returns:
        tuple: (lons, lats, times, slp)

    Examples:
        >>> lons, lats, times, slp = list_lon_lat_time_slp(pre_path)

    """
    win, mainhead = strip_fileheader(f)
    lons, lats, times, wss = [], [], [], []
    for step in return_steps_pre(win):
        lat, lon = getlatlon(step)
        time = step[0]["time"]
        ws = getslp(step)
        lons.append(lon)
        lats.append(lat)
        times.append(time)
        wss.append(ws)
    return lons, lats, times, wss


def iter_file(fpath, ftype=None):
    """Given a WIN/PRE filepath, return generator/iterable for timesteps.

    Args:
        fpath (str): Path to single WIN/PRE file.
        ftype (str): Force the filetype to either "win" or "pre". Defaults to None.

    Yields:
        tuple: providing (ftype, header, ws, u, v) or (ftype, header, pre)
        depending on the file type.

    Examples:
        >>> for data in iter_file(fpath, ftype):
        >>>     ftype = data[0]
        >>>     time = data[1]["time"]
        >>>     lat, lon = getlatlon_fromhead(data[1])

    """
    if ftype is None:
        ftype = os.path.splitext(fpath)[-1][1:].lower()
    with open(fpath) as winpre:
        windat, mainhead = strip_fileheader(winpre)
        if ftype == "win":
            for step in return_steps_win(windat):
                head, data = step
                uv = getuv(step)
                ws = mags(uv)
                yield (ftype, head, ws, *uv)
        else:
            for step in return_steps_pre(windat):
                head, data = step
                pre = getslp(step)
                yield (ftype, head, pre)


def iter_file_xrdataset(fpath, ftype=None):
    """Given a WIN/PRE filepath, return generator/iterable for timesteps that
    outputs xarray.Datasets.

    Args:
        fpath (str): Path to single WIN/PRE file.
        ftype (str): Force the filetype to either "win" or "pre". Defaults to None.

    Yields:
        xarray.Dataset: For timestep

    Examples:
        >>> wind = xr.concat(
        >>>     list(iter_file_xrdataset(inwin, ftype="win")),
        >>>     dim="time",
        >>>     coords="different",
        >>>     data_vars="different",
        >>> )
        >>> pres = xr.concat(
        >>>     list(iter_file_xrdataset(inpre, ftype="pre")),
        >>>     dim="time",
        >>>     coords="different",
        >>>     data_vars="different",
        >>> )

    """
    dims = ("time", "yi", "xi")
    for data in iter_file(fpath, ftype):
        ftype = data[0]
        time = data[1]["time"]
        lat, lon = getlatlon_fromhead(data[1])
        ds = xr.Dataset()
        ds["lat"] = xr.DataArray(lat.astype(np.float32), dims=("yi", "xi"))
        ds["lon"] = xr.DataArray(lon.astype(np.float32), dims=("yi", "xi"))
        ds["lon"].attrs = {
            "units": "degrees_east",
            "standard_name": "longitude",
            "axis": "X",
            "coordinates": "time lat lon",
        }
        ds["lat"].attrs = {
            "units": "degrees_north",
            "standard_name": "latitude",
            "axis": "Y",
            "coordinates": "time lat lon",
        }
        ds["time"] = xr.DataArray([pd.to_datetime(time)], dims="time")
        ds["time"].attrs = {"axis": "T", "coordinates": "time"}
        if ftype == "win":
            # ds["WS"] = xr.DataArray(
            #    data[2].reshape((1, *ds.lat.shape)), dims=dims, type=np.float32
            # )
            ds["U10"] = xr.DataArray(
                data[3].reshape((1, *ds.lat.shape)).astype(np.float32), dims=dims
            )
            ds["V10"] = xr.DataArray(
                data[4].reshape((1, *ds.lat.shape)).astype(np.float32), dims=dims
            )
            # ds["WS"].attrs = {"units": "m s-1", "coordinates": "time lat lon"}
            ds["U10"].attrs = {"units": "m s-1", "coordinates": "time lat lon"}
            ds["V10"].attrs = {"units": "m s-1", "coordinates": "time lat lon"}
        else:
            ds["PSFC"] = xr.DataArray(
                data[2].reshape((1, *ds.lat.shape)).astype(np.float32), dims=dims
            )
            ds["PSFC"].attrs = {"units": "mb", "coordinates": "time lat lon"}
        yield ds


def getuv(step):
    """From WINPRE timestep tuple, return u and v data.

    Args:
        step (tuple): Winpre timestep tuple (head, data).

    Returns:
        tuple: (u, v).

    Examples:
        >>> for step in return_steps_win(win):
        >>>     lat, lon = getlatlon(step)
        >>>     time = step[0]["time"]
        >>>     ws = getuv(step)
        >>>     ws = mags(ws)

    """
    head, data = step
    length_grid = head["numlat"] * head["numlong"]
    # print length_grid
    u = data[:length_grid]  # Check here for data
    v = data[length_grid:]  # Check here for data
    assert len(u) == length_grid
    assert len(v) == length_grid
    # print np.where(np.isnan(u))
    assert not np.any(np.isnan(u))
    assert not np.any(np.isnan(v))
    return u, v


def getslp(step):
    """From WINPRE timestep tuple, return slp data.

    Args:
        step (tuple): Winpre timestep tuple (head, data).

    Returns:
        array: Pressure data array.

    Examples:
        >>> for step in return_steps_win(win):
        >>>     lat, lon = getlatlon(step)
        >>>     time = step[0]["time"]
        >>>     pre = getslp(step)

    """
    head, data = step
    length_grid = head["numlat"] * head["numlong"]
    # print length_grid
    slp = data[:length_grid]  # Check here for data
    assert len(slp) == length_grid
    assert not np.any(np.isnan(slp))
    return slp


def getlatlon(step):
    """From WINPRE timestep tuple, return lat and lon coordinates.

    Args:
        step (tuple): Winpre timestep tuple (head, data).

    Returns:
        tuple: (lat, lon).

    Examples:
        >>> win, mainhead = strip_fileheader(f)
        >>> for step in return_steps_win(win):
        >>>     lat, lon = getlatlon(step)

    """
    head, data = step
    numlong = head["numlong"]
    numlat = head["numlat"]
    swlat = head["swlat"]
    swlon = head["swlon"]
    dlat = head["dlat"]
    dlong = head["dlong"]
    lat = np.zeros((numlat, numlong))
    lon = np.zeros((numlat, numlong))
    for ilon in range(0, numlong):
        for ilat in range(0, numlat):
            lat[ilat, ilon] = swlat + ilat * dlat
            lon[ilat, ilon] = swlon + ilon * dlong
    return lat, lon


def getlatlon_fromhead(head):
    """From WINPRE timestep header dict, return lat and lon coordinates.

    Args:
        head (dict): Winpre timestep header dictionary.

    Returns:
        tuple: (lat, lon).

    Examples:
        >>> for data in iter_file(fpath, ftype):
        >>>     ftype = data[0]
        >>>     time = data[1]["time"]
        >>>     lat, lon = getlatlon_fromhead(data[1])

    """
    numlong = head["numlong"]
    numlat = head["numlat"]
    swlat = head["swlat"]
    swlon = head["swlon"]
    dlat = head["dlat"]
    dlong = head["dlong"]
    lon = np.linspace(swlon, swlon + (numlong * dlong), endpoint=True, num=numlong)
    lat = np.linspace(swlat, swlat + (numlat * dlat), endpoint=True, num=numlat)
    lon, lat = np.meshgrid(lon, lat)
    return lat, lon


def mags(uv):
    """Calculate magnitude from u/v components provided in tuple as (u, v)

    Args:
        uv: (u, v) tuple

    Returns:
        magnitude (float): `(sqrt(u**2 + v**2))`

    """
    u, v = uv
    return np.sqrt((u ** 2) + (v ** 2))


def floorspeed(uv, thresh=None):
    thresh = float(thresh)
    if threshold is None:
        return uv
    else:
        speed = mags(uv)
        i = speed < thresh
        f = thresh / speed[i]
        u, v = uv
        u[i] = u[i] * f
        v[i] = v[i] * f
        return u, v


def capspeed(uv, cap=None):
    cap = float(cap)
    if threshold is None:
        return uv
    else:
        speed = mags(uv)
        i = speed > cap
        f = speed[i] / cap
        u, v = uv
        u[i] = u[i] * f
        v[i] = v[i] * f
        return u, v


def uv2str(uv):
    u, v = uv
    # utxt = StringIO.StringIO()
    # vtxt = StringIO.StringIO()
    numlines_full, remainder = np.floor(u.shape[0] / num_col), u.shape[0] % num_col
    fmt = ((output_array_fmt * num_col) + "\n") * numlines_full
    fmt += (output_array_fmt * remainder) + "\n"
    # np.savetxt(utxt, u, fmt=fmt)
    # np.savetxt(vtxt, v, fmt=fmt)
    utxt = fmt % tuple(u)
    vtxt = fmt % tuple(v)
    return utxt + vtxt


def to_nws13_group(output, group, rank, wind=None, pre=None, mode="w"):
    """Write win/pre data out to netcdf adcirc NWS13 formatted group.

    Args:
        output (str): Output netcdf filepath.
        group (str): Group name.
        rank (int): Group rank.
        wind (xarray.Dataset): Dataset with U10, V10 (and possibly PSFC), and coordinates.
        pre (xarray.Dataset): Dataset with PSFC varaible. Defaults to None.
        mode (str): Set to "a" to append to exisiting file, "w" will overwrite. Defaults to "w".

    Returns:
        None

    Examples:
        >>> mode = "w"
        >>> rank = 1
        >>> outgroups = ingroups
        >>> for inwin, inpre, group in zip(inwinfiles, inprefiles, ingroups):
        >>>     wind = xr.concat(
        >>>         list(iter_file_xrdataset(inwin, ftype="win")),
        >>>         dim="time",
        >>>         coords="different",
        >>>         data_vars="different",
        >>>     )
        >>>     to_nws13_group(output, group, rank, wind=wind, mode=mode)
        >>>     mode = "a"
        >>>     del wind
        >>>     pres = xr.concat(
        >>>         list(iter_file_xrdataset(inpre, ftype="pre")),
        >>>         dim="time",
        >>>         coords="different",
        >>>         data_vars="different",
        >>>     )
        >>>     to_nws13_group(output, group, rank, pre=pres, mode=mode)
        >>>     rank += 1
        >>>     del pres

    """
    if wind is not None:
        if "WS" in wind:
            del wind["WS"]
        wind.attrs["rank"] = np.int32(rank)
        wind.time.encoding = {
            "units": "minutes since 1990-01-01 01:00:00",
            "dtype": int,
        }
        wind.to_netcdf(output, group=group, mode=mode)
    if pre is not None:
        pre.time.encoding = {
            "units": "minutes since 1990-01-01 01:00:00",
            "dtype": int,
        }
        pre["PSFC"].to_netcdf(output, group=group, mode="a")


if __name__ == "__main__":

    # Samples for Reading Win/Pre Files

    winfile = "O:/Python/Sub/owiWinPre/201304_1DegNATL.Win"
    with open(winfile) as win:
        win, mainhead = strip_fileheader(win)
        for step in return_steps_win(win):
            head, data = step
            uv = getuv(step)
            ws = mags(uv)
            print(
                "WIN Reading "
                + head["time"].isoformat()
                + " Maximum Wind Speed (m/s):",
                np.amax(ws),
            )
    print("Win Complete")

    prefile = "O:/Python/Sub/owiWinPre/201304_1DegNATL.Pre"
    with open(prefile) as pre:
        pre, mainhead = strip_fileheader(pre)
        for step in return_steps_pre(pre):
            head, data = step
            slp = getslp(step)
            print(
                "PRE Reading "
                + head["time"].isoformat()
                + " Minimum Sea Level Pressure (mb):",
                np.amin(slp),
            )
    print("Pre Complete")
