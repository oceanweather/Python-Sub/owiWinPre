.. OWI Win/Pre documentation master file, created by
   sphinx-quickstart on Fri Jan 22 22:13:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OWI Win/Pre's documentation!
=======================================

This Python module provides an interfact to Oceanweather Inc's. ascii .WIN/.PRE
wind and pressure field data format. An interface to read .WIN/.PRE files into
xarray objects is included.

.. toctree::
   :maxdepth: 2
   :caption: Module API:

   modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
